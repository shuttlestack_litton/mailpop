from flask import Flask, render_template

app = Flask(__name__)

# TODO: // login system with Flask-WTForms and Flask-SQLAlchemy (URL: https://www.youtube.com/watch?v=8aTnmsDMldY) // push to Heroku
# NOTES: // Mailpop will have 100+ email templates on display for users to implement. // Easily pop designs into Mailchimp, Mailgun, and SendGrid. // Simple but beautiful UI with great animations and detail. // When a button is clicked that calls to the server, remove the test and place a loader on it. // Content should also be loaded either as a content placeholder (slack) or nprogress (make this actually render a load, not for looks.)

@app.route("/")
def index():
    return render_template('home.html', title='Home')

@app.route("/login")
def login():
	return render_template('home.html', title='Login')

if __name__ == '__main__':
	app.run(debug=True)